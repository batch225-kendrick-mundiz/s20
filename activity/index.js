let num = parseInt(prompt("Please input a number"));


for(x = num; x >= 0; x--){
    if(x > 50){
        if(x%10 == 0){
            console.log("Number is divisible by 10, skipping number");
        }else if(x%5 == 0){
            console.log(x);
        }
    }else{
        console.log("Number is less than 50, terminating the loop");
    }
    
}

let myString = "supercalifragilisticexpialidocious";
let myStringConsonant = "";

for (x = 0; x < myString.length; x++){
    if(myString[x].toLowerCase() != "a" && myString[x].toLowerCase() != "e" && myString[x].toLowerCase() != "i" && myString[x].toLowerCase() != "o" && myString[x].toLowerCase() != "u"){
        myStringConsonant = myStringConsonant + myString[x];
    }
}
console.log(myString);
console.log(myStringConsonant);